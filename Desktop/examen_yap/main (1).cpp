#include <iostream>
#include <fstream>
#include <string>
#include <math.h>




int GotKubeSum(int n, int numbers[8]) {
	numbers[0] = cbrt(n);
	int sum = numbers[0] * numbers[0] * numbers[0];
	if (sum == n) {
		std::cout << numbers[0];
	}
	else {
		for (int i = 1; i < 8; ++i) {
			if (sum + cbrt(numbers[i - 1] * numbers[i - 1] * numbers[i - 1]) < n) {
				numbers[i] = cbrt(numbers[i - 1] * numbers[i - 1] * numbers[i - 1]);
				sum = sum + numbers[i] * numbers[i] * numbers[i];
			}
			/*else {
				numbers[i] = cbrt((numbers[i - 1]-1) * (numbers[i - 1]-1) * (numbers[i - 1]-1));
				sum = sum + numbers[i] * numbers[i] * numbers[i];
			}*/
			if (sum > n) {
				break;
			}
		}


		if (sum > n) {
			std::cout << "IMPOSSIBLE";
		}
		else {
			for (int i = 0; i < 8; ++i) {
				if (numbers[i] > 0) {
					std::cout << numbers[i] << " ";
				}
			}
		}
	}
	return 0;
}


struct Application
{
	Application* next; 
	Application* prev; 

	std::string applicationName;
};

bool IsRunCommand(const std::string& line)
{
	return line.rfind("Run", 0) == 0; 
}

std::string GetApplicationName(const std::string& command)
{
	return command.substr(4, command.size()); 
}

int CountTabs(const std::string& line)
{
	int count = 0;
	for (int i = 0; i < line.size(); ++i)
	{
		if (line[i] == '+')
		{
			++count;
		}
	}
	return count;
}

int main()
{
	setlocale(LC_ALL, "Russian");
	std::cout << "������� 1. (����� � ����)" << std::endl;
	
	int n;
	std::string line;

	std::ifstream in("input.txt");  
	std::ofstream out("output.txt");
	
	in >> n;
	getline(in, line); 
	getline(in, line); 

	Application* activeApplication = new Application(); 
	activeApplication->applicationName = GetApplicationName(line);
	activeApplication->next = activeApplication;
	activeApplication->prev = activeApplication; 
	out << activeApplication->applicationName << "\n";

	int activeApplicationsCount = 1;

	for (int i = 1; i < n; ++i)
	{
		
		getline(in, line);
		if (IsRunCommand(line))
		{
			++activeApplicationsCount; 

			Application* newApplication = new Application();
			newApplication->applicationName = GetApplicationName(line);
			newApplication->next = activeApplication; 
			
			Application* lastApplication = activeApplication->prev;
			lastApplication->next = newApplication; 
			newApplication->prev = lastApplication; 

			activeApplication->prev = newApplication; 
			activeApplication = newApplication; 
			out << activeApplication->applicationName << "\n";
		}
		else
		{
			int countTabs = CountTabs(line);
			countTabs %= activeApplicationsCount; 

			for (int i = 0; i < countTabs; ++i)
			{
				activeApplication = activeApplication->next;
			}

			out << activeApplication->applicationName << "\n";
		}
	}

	in.close();
	out.close();

	
	int N;
	int numbers[8];
	for (int i = 0; i < 8; ++i) {
		numbers[i] = 0;
	}
	std::cout << "������� 2." << std::endl << "������� ����� n:";
	std::cin >> N;
	GotKubeSum(N, numbers);


	return 0;
}