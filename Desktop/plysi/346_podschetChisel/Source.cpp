#include <iostream>

int main() {
	int n, a, nul, otr, pol;
	std::cin >> n;
	nul = 0;
	otr = 0;
	pol = 0;
	for (int i = 0; i < n; i++) {
		std::cin >> a;
		if (a < 0) { otr = otr + 1; }
		if (a == 0) { nul = nul + 1; }
		if (a > 0) { pol = pol + 1; }
	}
	std::cout << nul << " " << pol << " " << otr;
	system("pause");
	return 0;
}