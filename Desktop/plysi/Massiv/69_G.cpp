#include <iostream>

int main() {
	int arr[35];
	int n, k;
	k = 0;
	std::cin >> n;
	for (int i = 0; i < n; i++) {
		std::cin >> arr[i];
	}
	for (int i = 0; i < (n / 2) ; i++) {
		k = arr[i];
		arr[i] = arr[(n - i) - 1];
		arr[(n - i) - 1] = k;
	}
	for (int i = 0; i < n; i++) {
		std::cout << arr[i] << " ";
	}
	system("pause");
	return 0;
}
