#include <Iostream>

int main() {
	int arr[10000];
	int n;
	int k = 0;
	std::cin >> n;
	for (int i = 1; i < n; i++) {
		std::cin >> arr[i];
	}
	for (int i = 1; i < n-1; i++) {
		if (((arr[i] > 0) && (arr[i + 1] > 0)) || ((arr[i] < 0) && (arr[i + 1] < 0))) {
			k = 1;
			std::cout << "YES";
			exit(0);
		}
	}
	std::cout << "NO";
	system("pause");
	return 0;
}