#include <iostream>

int main() {
	int arr[35];
	int n;
	int k;
	k = 0;
	std::cin >> n;
	for (int i = 0; i < n; i++) {
		std::cin >> arr[i];
	}
	for (int j = 1; j < n; j+=2) {
		k = arr[j];
		arr[j] = arr[j - 1];
		arr[j - 1] = k;
	}
	for (int i = 0; i < n; i++) {
		std::cout << arr[i] << " ";
	}
	system("pause");
	return 0;
}
