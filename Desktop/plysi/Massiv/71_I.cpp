
#include <iostream>

int main() {
	int arr[35];
	int n;
	int k;
	std::cin >> n;
	for (int i = 0; i < n; i++) {
		std::cin >> arr[i];
	}
	k = arr[n-1];
	for (int i = n-1; i > 0; i--) {
		arr[i] = arr[i - 1];
	}
	arr[0] = k;
	for (int i = 0; i < n; i++) {
		std::cout << arr[i] << " ";
	}
	system("pause");
	return 0;
}
