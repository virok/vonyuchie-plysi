#include <iostream>

int main() {
	int column;
	int line;
	std::cin >> column >> line;
	int** matrix = new int* [column];

	for (int i = 0; i < column; ++i) {
		matrix[i] = new int[line];
	}
	int k = 0;
	for (int i = 0; i < line; ++i) {
		for (int j = 0; j < column; ++j) {
			std::cin >> matrix[i][j];
		}
	}
	int max = -1;
	int max_line = -1;
	int max_1 = 0;
	for (int i = 0; i < line; ++i) {
		max_1 = 0;
		for (int  j = 0; j < column; ++j) {
			 max_1 = max_1 + matrix[i][j];
		}
		if (max_1 > max) {
			max = max_1;
			max_line = i;
		}
	}
	std::cout << max << std::endl;
	std::cout << max_line;

	for (int i = 0; i < column; ++i) {
		delete matrix[i];
	}



	delete[] matrix;

	return 0;
}
