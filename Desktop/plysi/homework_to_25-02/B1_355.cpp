#include <iostream>

int main() {
	int n;
	std::cin >> n;
	int** matrix = new int* [n];

	for (int i = 0; i < n; ++i) {
		matrix[i] = new int[n];
	}
	int k = 0;
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			std::cin >> matrix[i][j];
		}
	}
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			if ((j > i) || (j < i)) {
				if (matrix[i][j] == matrix[j][i]){
					k=k+1;
					}
			}
		}
	}
	if (k == n * n - n) {
		std::cout << "yes";
	}
	else {
		std::cout << "no";
	}

	for (int i = 0; i < n; ++i) {
		delete matrix[i];
	}



	delete[] matrix;

	return 0;
}
