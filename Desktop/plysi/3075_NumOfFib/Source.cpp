#include <iostream>

int main() {
	int f0, f1, f, n, i;
	std::cin >> n;
	f0 = 0;
	f1 = 1;
	i = 1;
	f = 0;
	if (n == 0) {
		std::cout << "0";
		exit(0);
	}
	if (n == 1) {
		std::cout << "1";
		exit(0);
	}
	while (f < n){
		f = f0 + f1;
		f0 = f1;
		f1 = f;
		i = i + 1;
	}
	if (f == n) {
		std::cout << i;
	}
	else {
		std::cout << "-1";
	}
	system("pause");
	return 0;
}
