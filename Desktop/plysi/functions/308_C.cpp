#include <iostream>

bool Xor(int x, int y) {
	bool flag;
	if (((x == 1) && (y == 0)) || ((x == 0) && (y == 0))) {
		return true;
	}
	else {
		return false;
	}
}

int main() {
	int x;
	int y;
	std::cin >> x >> y;
	std::cout << Xor(x, y);
	return 0;
}