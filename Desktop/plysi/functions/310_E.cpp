#include <iostream>

void Prime(int x) {
	int k = 1;
	for (int i = 2; i < x / 2; ++i) {
		if (x % i == 0) {
			k = k + 1;
		}
		if (k > 1) {
			std::cout << "composite";
			break;
		}
	}
	if (k == 1) {
		std::cout << "prime";
	}
}

int main() {
	int x;
	std::cin >> x;
	Prime(x);
	return 0;
}