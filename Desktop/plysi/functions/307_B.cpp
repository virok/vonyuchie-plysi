#include <iostream>

void DoublePower(double &a, int &n){
	int a1 = a;
	if (n == 0) {
		a = 1;
	}
	else {
		for (int i = 1; i < n; ++i) {
			a = a * a1;
		}
	}
}

int main() {
	double a;
	int n;
	std::cin >> a >> n;
	DoublePower(a, n);
	std::cout << a;
	return 0;
}