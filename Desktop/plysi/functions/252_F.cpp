#include <iostream>


void Result(double& a, int& n) {
	if (n == 0) {
		a = 1;
	}else if (n > 0) {
		int a1 = a;
		for (int i = 1; i < n; ++i) {
			a = a * a1;
		}
	}else if (n < 0) {
		n = n * (-1);
		int a1 = a;
		for (int i = 1; i < n; ++i) {
			a = a * a1;
		}
		a = 1 / a;
	}
}

int main() {
	double a;
	int n;
	std::cin >> a >> n;
	Result(a, n);
	std::cout << a;
	return 0;
}
