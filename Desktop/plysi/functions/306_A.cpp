#include <iostream>

void Min(int* array, int &min) {
	
	for (int i = 1; i < 4; ++i) {
		if (array[i] < min) {
			min = array[i];
		}
	}
}

int main() {
	int* array = new int[4];
	for (int i = 0; i < 4; ++i) {
		std::cin >> array[i];
	}
	int min = array[0];
	Min(array, min);
	std::cout << min;
	delete[]array;
	return 0;
}