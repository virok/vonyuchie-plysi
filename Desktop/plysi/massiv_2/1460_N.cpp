#include <iostream>

int main() {
	int n;
	std::cin >> n;
	int* array = new int[n];
	for (int i = 0; i < n; i++) {
		std::cin >> array[i];
	}
	int k;
	std::cin >> k;
	if (k > 0) {
		while (k > 0) {
			int remember_element = array[n - 1];
			for (int i = n-1; i > 0; --i) {
				array[i] = array[i - 1];
			}
			array[0] = remember_element;
			k = k - 1;
		}
	} else {
		k = k * (-1);
		while (k > 0) {
			int remember_element = array[0];
			for (int i = 0; i < n; ++i) {
				array[i] = array[i + 1];
			}
			array[n-1] = remember_element;
			k = k - 1;
		}
	}
	for (int i = 0; i < n; ++i) {
		std::cout << array[i] << " ";
	}
	delete[] array;
	return 0;
}
