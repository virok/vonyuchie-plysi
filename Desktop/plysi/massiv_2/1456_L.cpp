#include <iostream>

int main() {
	int n;
	int number;
	int height_P;
	std::cin >> n;
	int* array = new int[n];
	for (int i = 0; i < n; ++i) {
		std::cin >> array[i];
	}
	std::cin >> height_P;
	for (int i = 0; i < n; ++i) {
		if (height_P < array[i]) {
			number = i + 2;
		}
	}
	std::cout << number;
	delete[] array;
	return 0;
}
