#include <iostream>

int main() {
	int n;
	std::cin >> n;
	int* array = new int[n];
	for (int i = 0; i < n; ++i) {
		std::cin >> array[i];
	}
	int max_amount_elements = 3;
	int sum = 0;
	while (max_amount_elements > 2) {

		int number_of_last = -1;

		for (int i = 1; i <= n - 2; ++i) {
			if ((array[i] == array[i - 1]) && (array[i] == array[i + 1])) {
				number_of_last = i + 1;
				break;
			}
		}

		int number_of_last_1 = number_of_last;
		int amount_elements = 3;

		for (int j = number_of_last_1; j < n - 1; ++j) {
			if (array[j] == array[j + 1]) {
				number_of_last = number_of_last + 1;
				amount_elements = amount_elements + 1;
			}
		}

		int amount_elements_1 = amount_elements;

		while (amount_elements_1 > 0) {
			for (int i = number_of_last - amount_elements + 1; i < n; ++i) {
				array[i] = array[i + 1];
			}
			amount_elements_1 = amount_elements_1 - 1;
		}
		 
		sum = sum + amount_elements;
		n = n - amount_elements;
		
		max_amount_elements = 1;

		for (int i = 1; i <= n - 2; ++i) {
			if ((array[i] == array[i - 1]) && (array[i] == array[i + 1])) {
				max_amount_elements = 3;
				break;
			}
		}

	}

	std::cout << sum;

	delete[] array;
	return 0;
}
