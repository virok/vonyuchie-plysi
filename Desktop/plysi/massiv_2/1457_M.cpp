#include <iostream>

int main() {
	int n;
	int a;
	int b;
	int c;
	int d;
	std::cin >> n >> a >> b >> c >> d;

	int* array = new int[n];

	for (int i = 0; i < n; ++i) {
		array[i] = i + 1;
	}
	for (int i = a - 1; i < a + (b - a) / 2 - 1; ++i) {
		int swap_helper;
		swap_helper = array[i];
		array[i] = array[b + a - i - 2]; 
		array[b - i - 1] = swap_helper;
	}
	for (int i = c - 1; i < c + (d - c) / 2 - 1; ++i) {
		int swap_helper;
		swap_helper = array[i];
		array[i] = array[d + c - i - 2];
		array[d - i - 1] = swap_helper;
	}
	for (int i = 0; i < n; ++i) {
		std::cout << array[i] << " ";
	}
	delete[] array;

	return 0;
}