#include <iostream>

int main() {
	int n;
	int k;
	int element;
	std::cin >> n;
	int* array = new int[n];
	for (int i = 0; i < n; ++i) {
		std::cin >> array[i];
	}
	k = 1;
	element = array[0];
	for (int i = 1; i < n; ++i) {
		if (array[i] > element) {
			element = array[i];
			k = k + 1;
		}
	}
	std::cout << k;
	delete[] array;
	return 0;
}