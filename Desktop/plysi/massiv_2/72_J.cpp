#include <iostream>

int main() {
	int n;
	int max;
	std::cin >> n;
	int* array = new int[n]; 
	for (int i = 0; i < n; ++i) {
		std::cin >> array[i];
	}
	max = array[0];
	for (int i = 0; i < n; ++i) {
		if (array[i] > max) {
			max = array[i];
		}
	}
	std::cout << max;
	delete[] array;
	return 0;
}