#pragma once
#include <iostream>
#include <math.h>
#include "Point.h"

class Square
{	
	
public:
	double diagonal = 0;
	double side = 0;
	Point left_bottom;
	Point right_top;
	Point left_top;
	Point right_bottom;
	Square(Point p1, Point p2) : left_bottom(p1), right_top(p2) {};
	Square(const Square& square) : left_bottom(square.left_bottom), right_top(square.right_top) {};
	double GetAbs() {
		diagonal = sqrt((left_bottom.x - right_top.x) * (left_bottom.x - right_top.x) + (left_bottom.y - right_top.y) * (left_bottom.y - right_top.y));
		return diagonal;
	}
	double GetSide() {
	    side= diagonal / sqrt(2);
		return side;
	}
	Point GetLeftTop() {
		Point left_top(left_bottom.x, left_bottom.y + side);
		return left_top;
	}
	Point GetRightBottom() {
		Point right_bottom(right_top.x, right_top.y - side);
	}
	double GetSquare() {
		return side*side;
	}
	
};

