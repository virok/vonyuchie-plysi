#pragma once
#include <math.h>
#include <iostream>

class Point
{
	
public:
	double x;
	double y;
	Point() {};
	Point(double x1, double y1) :x(x1), y(y1) {};
	/*Point(double x1, double y1) {
		x = x1;
		y = y1;
	}*/
	Point(const  Point& point) :x(point.x), y(point.y) {};
};

